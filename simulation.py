#-*- coding: utf-8 -*-
import math, random, sys, time
import numpy as np

def getDigits(filename):
    '''
    Récupère les décimales de pi.
    @param filename: le nom du fichier contenant pi.
    @return: une liste contenant chaque décimale de pi comme étant un élément 
    de cette liste et le string contenant toutes les décimales.
    '''
    with open(filename, 'r') as pif:
        digits = ''.join(pif.read()[2:].splitlines())
    return [int(i) for i in digits], digits
    
def getClasses(digits, a, b, n):
    '''
    Crée les classes nécessaires à la réalisation du test du chi carré.
    @param digits: les digits à classer.
    @param a: le minimum parmi les classes à créer.
    @param b: le maximum parmi les classes à créer.
    @param n: le nombre de classes.
    @return: la liste des classes.
    '''
    interval = (b - a) / float(n)
    classes = [0] * n
    for d in digits:
        classToIcrmt = int(d / interval)
        classes[classToIcrmt] += 1
    return classes

def evaluateChiSquare(classes, f_exp = None):
    '''
    Donne la valeur retournée par le test du chi carré effectué sur les 
    classes passées en paramètres. Source : scipy.stats.chisquare
    url : http://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.
    chisquare.html#scipy.stats.chisquare
    @param classes: les classes à fournir pour effectuer le test.
    @param f_exp: les fréquences attendues pour chaque catégorie.
    @return: le résultat du test.
    '''
    f_obs = np.asarray(classes)
    k = len(f_obs)
    
    if f_exp is None:
        f_exp = np.array([np.sum(f_obs,axis=0)/float(k)] * len(f_obs),float)
    else:
        f_exp = np.asarray(f_exp)
    
    f_exp = f_exp.astype(float)
    chisq = np.add.reduce((f_obs-f_exp)**2 / f_exp)
    return chisq

def evaluateGap(digits, k, n):
    '''
    Donne les valeurs observées et théoriques du test du Gap.
    @param digits: les digits sur lesquels effectuer le test.
    @param k: le digit considéré pour le test.
    @param n: le nombre de classes à utiliser.
    @return: les valeurs observées lors de l'application du test.
    '''
    observed = [0] * n
    i = 0
    for x in digits:
        if x != k:
            i += 1
        else:
            observed[i if i < n - 1 else -1] += 1
            i = 0
    return observed

def evaluateTheoricGap(digits, n):
    '''
    Donne les valeurs théoriques du test du Gap.
    @param digits: les digits sur lesquels effectuer le test.
    @param n: le nombre de classes à utiliser.
    @return: les valeurs théoriques du test.
    '''
    theoric = [len(digits) * 0.1 ** 2 * 0.9 ** i for i in xrange(n)]
    theoric[-1] = 0
    theoric[-1] = len(digits) / 10. - sum(theoric)
    return theoric

def genRandPython(seed = None):
    '''
    Renvoie un itérateur depuis le générateur de nombre pseudo-aléatoires
    flottants de Python.
    @param seed: la seed déterminant le départ de l'itérateur (par défaut,
    la seed est basée sur l'heure système).
    '''
    random.seed(seed)
    while True:
        yield random.random()

def genRandIntPi(digits, seed = None, length = 16):
    '''''
    Renvoie un itérateur depuis un générateur de nombres pseudo-aléatoires 
    entiers basé sur les décimales de pi.
    @param digits: les décimales de pi.
    @param seed: la seed déterminant le départ de l'itérateur (par défaut,
    la seed est basée sur l'heure système).
    @param length: le nombre de décimales utilisées pour générer chaque nombre
    (par défaut 16).
    '''
    current = int(time.time() * 1000) % len(digits) if seed is None \
              else seed % len(digits)
    while True:
        next = (current + length) % len(digits)
        if current < next:
            yield int(digits[current:next])
        else:
            yield int(digits[current:] + digits[:next])
        current = next
        
def genRandPi(digits, seed = None, length = 16):
    '''''
    Renvoie un itérateur depuis un générateur de nombres pseudo-aléatoires 
    flottants basé sur les décimales de pi.
    @param digits: les décimales de pi.
    @param seed: la seed déterminant le départ de l'itérateur (par défaut,
    la seed est basée sur l'heure système).
    @param length: le nombre de décimales utilisées pour générer chaque nombre
    (par défaut 16).
    '''
    gen = genRandIntPi(digits, seed, length)
    while True:
        yield gen.next() / (10. ** length)
        
def mkChiSquareTest(digits):
    '''
    Effectue et affiche les résultats du test du Chi carré.
    @param digits: la liste des décimales de pi.
    '''
    print "Test du Chi carré avec une classe par digit"
    print "-------------------------------------------"
    # On utilise ici une classe par digit, d'où 0, 10, 10
    classes = getClasses(digits, 0, 10, 10)
    print "Les classes correspondant à chaque digit : "
    print classes
    
    chiSquare = evaluateChiSquare(classes)
    print "Le test du Chi carré (9 degrés de liberté) renvoie " + \
            str(chiSquare)
    print
    
def mkGapTest(digits):
    '''
    Effectue et affiche les résultats du test du Gap.
    @param digits: la liste des décimales de pi.
    '''
    print "Test du Gap"
    print "-----------"
    n = int(math.log(1000. / len(digits)) / math.log(0.9)) + 1
    print "Nombre de classes idéalement choisi pour pouvoir effectuer"
    print "un Chi carré sur les données obtenues : " + str(n)
    theoric = evaluateTheoricGap(digits, n)
    dof = len(theoric) - 1
    print "Résultats du Chi carré sur les données du test du Gap,"
    for i in xrange(10):
        obs = evaluateGap(digits, i, n)
        chiSqGap = evaluateChiSquare(obs, theoric)
        print ("pour la décimale %d : %f" % (i, chiSqGap))
    print "Nombre de degrés de liberté : " + str(dof)
    print
    
def mkCompareGen(digits, strDigits):
    '''
    Compare les deux générateurs et affiche les résultats de la comparaison.
    @param digits: la liste des décimales de pi.
    @param strDigits: le string contenant les décimales de pi.
    '''
    print "Comparaison des générateurs de nombres pseudo-aléatoires"
    print "--------------------------------------------------------"
    # Si aucune quantité de nombres à générer n'est entrée en paramètres,
    # on en génère len(digits), pour avoir l'ensemble des valeurs possibles.
    if len(sys.argv) < 3:
        nToGen = len(digits)
    else:
        nToGen = int(sys.argv[2])
    
    def printChiSqFromGen(nToGen):
        genPy = genRandPython()
        genPi = genRandPi(strDigits)
        
        randomPy = [genPy.next() for i in xrange(nToGen)]
        randomPi = [genPi.next() for i in xrange(nToGen)]
        
        chiSqPy = evaluateChiSquare(getClasses(randomPy, 0, 1, 100))
        chiSqPi = evaluateChiSquare(getClasses(randomPi, 0, 1, 100))
        
        print "Résultats du Chi carré (99 degrés de liberté) à partir : "
        print ("du générateur Python - %d nombres générés : %f" % \
               (nToGen, chiSqPy))
        print ("du générateur basé sur pi - %d nombres générés : %f" % \
               (nToGen, chiSqPi))
        print
        
    printChiSqFromGen(nToGen)
    printChiSqFromGen(nToGen * 10)

if __name__ == '__main__':
    if len(sys.argv) < 2:
        print "Entrez en paramètre le nom du fichier contenant pi."
    else:
        digits, strDigits = getDigits(sys.argv[1])
        
        # Test du chi carré.
        mkChiSquareTest(digits)
        
        # Test du Gap
        mkGapTest(digits)
        
        # Comparaison des générateurs de nombres pseudo-aléatoires.
        mkCompareGen(digits, strDigits)